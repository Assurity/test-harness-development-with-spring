# Test Automation with Spring beyond dependency injection

### Usage

Please feel free to fork the project if you are wanting a base to start a new Test harness for testing of a service or have a go on your own following this tutorial.

### Overview
This demo will focus on considerations around design and how we built the automation framework for the DX micro-services project at IRD. Focus was driven by a need for maintainability, extendability, easily configurable, distribution and speed of development.

As the original scope of the project gave a time frame of just one month for development and test we needed to be able to stand up a test framework and emulation service very quickly. Therefore we needed something which was light weight on the boiler code, but could easily be extended should new services require different means of communication or different means of testing.

To fulfill these requirements Spring was chosen as a base framework. Spring gives a lot of out-of-the-box functionality which we needed to have, but didn't want to spend the time setting up ourselves. Alongside this we incorporated cucumber plugin as our BDD reporting, "living documentation" of choice.

Note: This demo is with Spring version 1.5.10, a newer version (2.0.4) is available and highly recommended!

### Session 1: Creating a new Project

This first demo is an intro to test harness application development, where we guide you on how to setup the initial stages of a project and how to quickly release something runnable. We Have chosen Groovy as our main language (which runs in a jvm) and gradle as our build tool. Lets get started:

- In your ide of choice (im using Intellij), create a new project: **Gradle** project, with Groovy (will grab Java automatically)
- Set artifact details: GroupID: com.assurity.test  
                        ArtifactID: test-harness-development-with-spring  
                        version: 1.0-SNAPSHOT  
                        **TIP**: we need to do this because of artifact maven publishing conventions
- Use default Gradle wrapper or local Gradle
        **TIP**: A gradle wrapper is a distribution of gradle which allows you to use the build tool without having to install anything in your build environment prior to development starting . This allows for a quick setup of your build environment, but also, because its versioned, gives you a mutable environment based on your development needs, which wont change wherever you deploy or run the application from.
- Confirm project location and name.

### Where's Groovy!?

- Check the project setup in the ide is correct, that is, JDK8 is applied correctly as the SDK.

- In the libraries, only Java8 SDK has been added by the ide so far.  
        **TIP**: A build.gradle file is the main build instructions file (like a pom file for maven) which controls the construction of the development environment.
        
- A Gradle refresh will pull down all the defined dependencies as per the build.gradle file

- Build the project, it should build successfully. 
        **TIP**: Building the Project with Gradle will compile any source code found and load any classpath resources necessary, assemble any distributions, compile and run any specified unit tests and resources.

### Running, Distributing the Application

- Create the project structure using intellij Project structure modules (gradle refresh if any libs go missing) or by right clicking on the project root in the Project view and creating a new directory, with the structure: ``` src/main/groovy ``` 
- Add a new package under groovy by right clicking and selecting add package, with the structure: ``` com/assurity/test/ ```
- Add a new groovy class under com/assurity/test called: ``` Application.class ``` with a simple main method:
``` 
    public static void main newApp(String[] args) {
        System.out.println("Hello World")
    }
```
- Then run by how your ide would run a java app. (In intellij there is a play button in the left hand line number bar, next to the method signature.) This should print out "Hello World"
    **TIP**: Running Java apps from the ide is easy if you know how, but this is not so easy if not running from an ide. If we want to distribute this application, we need something make running it easier, and not with an ide. Maybe we can use our gradle build environment to run this?
    **TIP**: Gradle comes with a lot of flexibility, there are plugins which extend the functionality of gradle to do a lot more things, one of these plugins is the application plugin. This plugin facilitates creating an executable JVM application.  
- Add application plugin and mainClassName
```
    apply plugin: 'application'
    mainClassName = "path.to.MainClass"
```
- Gradle refresh clean and build and then 'run', now located under the 'application' tasks. Hello World should be printed out after running the 'run' task.

- The application plugin automatically brings with it another plugin called the 'distribution' plugin.
    **TIP**: Distributing an application means having an executable script or file like a shell, batch script or jar file, which can run externally to an ide on a system that has the correct environment. 
- If we do a gradle clean task, followed by a build task, you will notice in the project structure a build directory. In this directory a distributions folder has appeared and in it one zip and one tar files (these are compressed files). We Can run these as our application from command line.
 
- To run from the executable distribution or compressed file, uncompress, and run the following from your commandline in the directory containing the uncompressed directory:
```  test-harness-development-with-spring-1.0-SNAPSHOT/bin/test-harness-development-with-spring  ``` 
- "Hello World" will be printed on the console.
- Explore the unzipped file and notice we have a number of objects in the lib file called class files. These are all the required class files necessary for running our application. All triggered by the scripts located in the bin folder.

- Back in our ide you will notice another folder under the build directory called libs. In this directory is a java jar, another way of packaging a distribution. However when we try running this jar using the following execute jar command: ``` java -jar test-harness-development-with-spring-1.0-SNAPSHOT.jar ``` 
- But it **doesn't work**, this is because if you open the jar with an un-compression tool, you see it is missing most of the class files, like which we saw in the zip and tar files and so cannot run. 


### Session 2: Spring!

- We can enhance this further by adding spring nature to the application
- We do this by adding the following to the build file:
```
    buildscript {
        repositories {
            mavenCentral()
        }
        dependencies {
            classpath("org.springframework.boot:spring-boot-gradle-plugin:1.5.10.RELEASE")
        }
    }
```   
- We must define the spring plugin dependency in a build script because the Spring plugin is used by Gradle
- Apply the plugin: ```  apply plugin: 'org.springframework.boot'  ```
    **TIP**: A buildscript is used when something is needed to be done before the gradle build environment is started up, i.e. it is needed by gradle. In this case the plugin we are applying for spring requires a dependency to be available for gradle first, before it can configure the build environment.

- And the basic Spring framework dependencies:
```
    compile("org.springframework.boot:spring-boot-starter-test:1.5.10.RELEASE")
    compile("org.springframework.boot:spring-boot-starter-logging:1.5.10.RELEASE")
```
- Gradle refresh then Clean Build
- We can now run using **bootRun** task
- To run from the executable distribution or compressed file, first un-compress it then run the following from your shell in the directory containing the uncompressed directory:
```  sh test-harness-development-with-spring-1.0-SNAPSHOT/bin/test-harness-development-with-spring  ```

- We can also now, thanks to the spring framework, create distributable runnable artifact as a fat jar
- If we explore the jar we can see it now contains the required dependencies to run
- We can run it using java -jar command:
```  java -jar test-harness-development-with-spring-1.0-SNAPSHOT.jar ```

### Adding Spring nature to the Application

- Now we can add some spring nature to our application to enable spring goodness when running
- add the annotation ``` @SpringBootApplication ``` onto the application class
    **TIP**: The SpringBootApplication annotation is actually a wrapper annotation for 3 very impolrtant spring application annotations. These are:
    1) @EnableAutoConfiguration: enable Spring Boot’s auto-configuration mechanism, this a great way to get up and running quickly as spring does some magic to determine what beans it needs to configure for you.
    
    2) @ComponentScan: enable @Component scan on the package where the application is located. This means it will search for all classes on the class path defined in a package of the same name as the location of the class the @ComponentScan Annotation is on.
     
    3) @Configuration: allow to register extra beans in the context or import additional configuration classes where beans are defined
  
    
- We add ``` SpringApplication.run(Application.class, args) ``` to the main class to run as a spring application.  

- But when we run this way spring will start up a tomcat server, which isn't what we want to happen here.  

- We prevent this by using a spring interface called ```  implements CommandLineRunner  ``` to run as a java application rather than as a service.

- To show we can control what our spring application does, we will add the following:
```
    @Override
    public void run(String... args) throws Exception {
        System.out.println("Im running with spring!")
    }
```

### Session 3: Configuration with Spring

- We need to add one more dependency for dealing with .yml files
```  compile group: 'org.yaml', name: 'snakeyaml', version: '1.19'  ```
- If we now add an application.yml file under src/main/resources we can in the yml format specify a greeting:
```
    test:
      greeting: "hello"
```
- In our application class, to access this we ned to add the annotation:
```  @ConfigurationProperties(prefix = "test")  ```
- Notice the prefix we set 'test' which relates to the highest level we set in the application.yml file and is the root of the data map which spring loads from.
- To access the value in 'greeting' we need to specify a String to get it called the same thing
- Now if we add the greeting to our overridden run method Sys.out command we can show that we are accessing our configuration.

### System Properties

- We might find that when we are configuring spring that there are certain system properties that we will always need to set, and doing this every time from commandline is tedious. So we can define these as code in our build file.
- The application.yml we defined for our configuration is the default for Spring. But we can define other configuration files for spring to look for by adding to our build file:
```
    applicationDefaultJvmArgs=['-Dspring.config.name=application,testdata']
    tasks.withType(JavaExec) {
        systemProperty 'Dspring.config.name', 'application,testdata'
        systemProperties System.properties
    }
```
- This gives us the ability to configure spring as code.
- It also gives us the ability in this case to specify other configuration files for spring to look for.
- So if we have another configuration file called 'testdata.yml' we can specify this using the above spring system property.
- If we create the file and add some test data to it:
```
    test:
      data:
        "Jim":
          fname: "James"
```
- When we go back to our application class, we are still mapping our configuration to a root called 'test' and spring is smart enough to apply this across all defined configuration files it has deemed to use for this build configuration.
- So if we map the test data for the persona "Julie" which is under 'data' we need to define a new **Map** called 'data' in our application class.
- And we can now access the persona 'Julie' and the data associated with her by ``` data.Julie.fname ``` and update our sys.out by greeting Julie.

- Another feature of spring configuration is the active profiles hierarchy, which we can use to have unique configuration across many domains, simply by adding a postfix to the defined spring.config.name's we have defined e.g. application-dev.yml would over write application.yml if the spring.profiles.active was set to 'dev'
```  -Dspring.profiles.active=dev  ```

### Session 4: Cucumber and Spring

- Now that the basic framework for configuring, running and distributing our application test harness is done, we should probably look at adding some tests!
- Because at IR we have chosen to use cucumber, we wil add this in to our framework:
    - I will create a class called IntegrationTest to contain the cucumber nature, and copy across some code I prepared earlier...

- Then we add cucumber dependencies features and step code:
    * We need to add some cucumber dependencies:
```
    compile("info.cukes:cucumber-java8:1.2.5")
    compile("info.cukes:cucumber-spring:1.2.5")
    compile("com.github.mkolisnyk:cucumber-runner:1.3")
```
   * Create the structure of the project for tests and resources src/main/resources/features/featureFile.feature
   * Define a basic Feature and Scenario
   * Create the corresponding step code class and code for the given Scenario.

- We can still use cucumber to run by right clicking on the scenario at this point...

- To get Spring to run the tests we will need to make our Application a bit smarter.
- To do this we need to call Junit to run our cucumber tests, this requires us to pass the class containing the Cucumber Nature.
``` JUnitCore.main(CucumberTest.canonicalName) ```
- And we add a new class 'CucumberTest' with the Cucumber Run options...
```
@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(
        jsonReport = "build/cucumber-reports/cucumber.json",
        detailedReport = true,
        detailedAggregatedReport = true,
        outputFolder = "build/cucumber-reports")
@CucumberOptions(
        features = "classpath:features",
        plugin = ["html:build/cucumber-reports/html",
                "json:build/cucumber-reports/cucumber.json",
                "junit:build/cucumber-reports/cucumber-results.xml"
        ])
```
- Then we can run using bootRun...

- We can now configure our test with data just as before.
- Add the below annotations onto the stepCode class to enable spring configuration:
```
    @ConfigurationProperties(prefix = "test")
    @EnableAutoConfiguration
    @ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class)
```
- Note some new annotations being used here to enable us to access the configuration files outside of the spring application class.

### Adding a World as an injected dependency - managing Scenario state

- We can show dependency injection with Spring and also how Cucumber Scenario state is managed.
- To do this we can add a World class:
    ```
        @Component
        @Scope("cucumber-glue")
        class World {
            Scenario scenario
            String variable = "default"
        }
    ```
- Note the two annotations:
    * @Component is to signify a spring component otherwise known as a bean
    * @Scope gives a scope to this bean in this case "cucumber-glue" is the scope which is applied by this bean

- To be able to pass data from the main application class to the the rest of the project, we can implement a spring bean to handle a scope, which in this case is the scope cucumber-scope a customized scope for cucumber (part of the cucumber framework to connect to spring)
- We add the following bean to the Application class;
```
    @Bean
    public static CustomScopeConfigurer glueScopeConfigurer(){
        CustomScopeConfigurer toReturn = new CustomScopeConfigurer();
        toReturn.addScope("cucumber-glue", new GlueCodeScope());
        return toReturn;
    }
```
- In the step code class we need to add the annotation ``` @ComponentScan("com.ird.test") ``` this is because Spring needs to know where to look for the bean components you are trying to use in the step code, but are now in BootableHarnessCore
- Now we can add a new World object into the StepCode class and use the annotation on the Object itself like this:
    ```
        @Autowired
        World world
    ```
- We can manipulate the String variable as an example to show what happens to the World between cucumber scenarios...


## Glossary

Readings and further information:
   - application plugin
   https://docs.gradle.org/current/userguide/application_plugin.html
   - Spring gradle plugin
   https://spring.io/blog/2017/04/05/spring-boot-s-new-gradle-plugin
   - Sping application: @SpringBootApplication
   https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-using-springbootapplication-annotation.html
   - Configuration with: Spring @ConfigurationProperties
   https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html
   - Auto Configuration: @EnableAutoConfiguration
   https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-auto-configuration.html
   - @ContextConfiguration
   https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html
