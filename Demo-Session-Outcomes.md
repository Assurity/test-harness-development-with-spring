## Session Planning: Test Automation with Spring beyond dependency injection

**Session 1:**
- Introduction and back ground.
- New project setup.
- Running and Distributing the Application.

	Session Outcomes:
	- Can setup a new Gradle Project in Intellij.
	- Can use Gradle to build and configure a new project.
	- Understand how to run a Java/groovy based application.
	- How to package and run a distributable application, what the benefits are from this ability.


**Session 2:**
- Spring!
	* Spring setup in Gradle build file
	* Adding Spring nature to the application
	* Running with Spring

	Session Outcomes:
	- How to configure Spring dependencies
	- What spring gives us in terms of running and distributing our application
	- What Spring configuration hierarchy and profiles are and empower us with
	- What the annotation @SpringBootApplication is used for.

**Session 3:**
- Configuration with Spring	and access to System Properties
- Cucumber and Spring

	Session Outcomes:
	- Snake yaml and how spring gives us easy accessibility to config.
	- How Spring manages Cucumber inside its own container context.
	- How Spring maintains test state by using Cucumber scope to create a cucumber container inside of spring's one.
	- What the annotations @ConfigurationProperties, @ContextConfiguration and @EnableAutoConfiguration are used for.


**Session 4:**
- How Cucumber fits into a Spring Container: Scope.
- Dependency injection: A Cucumber World.

    Session Outcomes:
    - How Spring maintains a Cucumber Scenario state through Cucumber scope and World object.
    - Dependency injection made easy with Spring, the benefits of this around cleaner and leaner code.
    - What the annotations @Component, @Scope, @Bean and @Autowired are used for.


**Session 5:**
- Emulated Services with Spring MVC

- mvc pattern
- validation
- error responses
- using a harness to test emulated service

